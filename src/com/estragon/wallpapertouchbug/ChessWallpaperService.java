package com.estragon.wallpapertouchbug;



import java.util.Random;

import android.annotation.TargetApi;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.Loader.OnLoadCompleteListener;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.widget.Toast;

public class ChessWallpaperService extends WallpaperService {

	@Override
	public Engine onCreateEngine() {
		return new MyWallpaperEngine();
	}

	private class MyWallpaperEngine extends Engine {
		private final Handler handler = new Handler();
		private boolean visible = true;
		private final Runnable drawRunner = new Runnable() {
			@Override
			public void run() {
				draw(Color.CYAN);
			}

		};

		public MyWallpaperEngine() {
			handler.post(drawRunner);
			this.setTouchEventsEnabled(false);
			enableOffsetNotifications();
		}
		
		@TargetApi(15)
		private void enableOffsetNotifications() {
			if (android.os.Build.VERSION.SDK_INT >= 15) {
				setOffsetNotificationsEnabled(false);
		    }
		}
		
		

		@Override
		public void onOffsetsChanged(float xOffset, float yOffset,
				float xOffsetStep, float yOffsetStep, int xPixelOffset,
				int yPixelOffset) {
			super.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep,
					xPixelOffset, yPixelOffset);
			//Toast.makeText(getApplicationContext(), "Event onOffsetsChanged reçu alors qu'il devrait pas ..." , Toast.LENGTH_SHORT).show();
			//draw(true);
		}

		@Override
		public void onVisibilityChanged(boolean visible) {
			this.visible = visible;
			if (visible) {
				handler.post(drawRunner);
			} else {
				handler.removeCallbacks(drawRunner);
			}
		}

		@Override
		public void onSurfaceDestroyed(SurfaceHolder holder) {
			super.onSurfaceDestroyed(holder);
			this.visible = false;
			handler.removeCallbacks(drawRunner);
		}

		@Override
		public void onSurfaceChanged(SurfaceHolder holder, int format,
				int width, int height) {
			super.onSurfaceChanged(holder, format, width, height);
		}

		@Override
		public void onTouchEvent(MotionEvent event) {
			Random random = new Random();
			draw(Color.rgb(random.nextInt(254), random.nextInt(254), random.nextInt(254)));
		}

		private void draw(int color) {
			SurfaceHolder holder = getSurfaceHolder();
			Canvas canvas = null;
			try {
				canvas = holder.lockCanvas();
				if (canvas != null) {
					canvas.drawColor(color);
				}
			} finally {
				if (canvas != null)
					holder.unlockCanvasAndPost(canvas);
			}
			handler.removeCallbacks(drawRunner);
			if (visible) {
				handler.postDelayed(drawRunner, 1000 * 60);
			}
		}
		
	}
}
